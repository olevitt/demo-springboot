package fr.rathesh.springbook.springbooktest.storage;

import fr.rathesh.springbook.springbooktest.Pokemon;

import java.util.List;

public interface PokemonDAO {

    public Iterable<Pokemon> getMyPokemons();
    public void insererPokemon(Pokemon pokemon);
}
