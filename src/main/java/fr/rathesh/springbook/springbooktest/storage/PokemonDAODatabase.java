package fr.rathesh.springbook.springbooktest.storage;

import fr.rathesh.springbook.springbooktest.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Primary
public class PokemonDAODatabase implements PokemonDAO {

    @Autowired
    private PokemonRepository pokemonRepository;

    public Iterable<Pokemon> getMyPokemons() {
        return pokemonRepository.findAll();
    }

    /**
     *
     * @return la liste des pokemons tries ou null en cas d'erreur
     */
    public List<Pokemon> getPokemonsTries() {
        try {
            Iterable<Pokemon> pokemons = pokemonRepository.findAll();
            List<Pokemon> pokemonsTries = new ArrayList<>();
            pokemons.forEach(pokemon -> pokemonsTries.add(pokemon));
            Collections.sort(pokemonsTries);
            return pokemonsTries;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public void insererPokemon(Pokemon pokemon) {
        pokemonRepository.save(pokemon);
    }
}
