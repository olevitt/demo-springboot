package fr.rathesh.springbook.springbooktest.storage;

import fr.rathesh.springbook.springbooktest.Pokemon;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonRepository extends CrudRepository<Pokemon, Integer> {
}
