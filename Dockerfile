FROM openjdk:11-jre
ADD target/*.jar /app.jar
CMD ["java", "-jar","/app.jar"]